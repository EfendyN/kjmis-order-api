"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "barangs",
      [
        {
          nama: "Beras Premium",
          tipe_unit: "Kg",
        },
        {
          nama: "Beras Medium",
          tipe_unit: "Kg",
        },
        {
          nama: "Gula Pasir",
          tipe_unit: "Kg",
        },
        {
          nama: "Minyak Goreng",
          tipe_unit: "Lt",
        },
        {
          nama: "Mie Instan",
          tipe_unit: "Bks",
        },
        {
          nama: "Daging Kerbau",
          tipe_unit: "Kg",
        },
        {
          nama: "Daging Ayam",
          tipe_unit: "Kg",
        },
        {
          nama: "Telur Ayam",
          tipe_unit: "Kg",
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("barangs", null, {});
  },
};