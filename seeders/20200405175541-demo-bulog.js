"use strict";

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "kota_bulogs",
      [
        {
          kota: "KOTA BANDA ACEH",
          provinsi: "ACEH",
        },
        {
          kota: "Kota Medan",
          provinsi: "SUMATERA UTARA",
        },
        {
          kota: "Kota Padang",
          provinsi: "SUMATERA BARAT",
        },
        {
          kota: "Kota Pekan Baru",
          provinsi: "RIAU",
        },
        {
          kota: "KOTA JAMBI",
          provinsi: "JAMBI",
        },
        {
          kota: "Kota Palembang",
          provinsi: "SUMATERA SELATAN",
        },
        {
          kota: "KOTA BENGKULU",
          provinsi: "BENGKULU",
        },
        {
          kota: "Kota Bandar Lampung",
          provinsi: "LAMPUNG",
        },
        {
          kota: "Kota Pangkal Pinang",
          provinsi: "KEPULAUAN BANGKA BELITUNG",
        },
        {
          kota: "Kota Tanjung Pinang",
          provinsi: "KEPULAUAN RIAU",
        },
        {
          kota: "KOTA JAKARTA PUSAT",
          provinsi: "DKI JAKARTA",
        },
        {
          kota: "KOTA BANDUNG",
          provinsi: "JAWA BARAT",
        },
        {
          kota: "KOTA SEMARANG",
          provinsi: "JAWA TENGAH",
        },
        {
          kota: "KOTA YOGYAKARTA",
          provinsi: "DI YOGYAKARTA",
        },
        {
          kota: "KOTA SURABAYA",
          provinsi: "JAWA TIMUR",
        },
        {
          kota: "KOTA TANGERANG",
          provinsi: "BANTEN",
        },
        {
          kota: "KOTA DENPASAR",
          provinsi: "BALI",
        },
        {
          kota: "Kota Mataram",
          provinsi: "NUSA TENGGARA BARAT",
        },
        {
          kota: "Kota Kupang",
          provinsi: "NUSA TENGGARA TIMUR",
        },
        {
          kota: "Kota Pontianak",
          provinsi: "KALIMANTAN BARAT",
        },
        {
          kota: "Kota Palangkaraya",
          provinsi: "KALIMANTAN TENGAH",
        },
        {
          kota: "Kota Banjarmasin",
          provinsi: "KALIMANTAN SELATAN",
        },
        {
          kota: "Kota Samarinda",
          provinsi: "KALIMANTAN TIMUR",
        },
        {
          kota: "Kota Tanjung Selor",
          provinsi: "KALIMANTAN UTARA",
        },
        {
          kota: "Kota Manado",
          provinsi: "SULAWESI UTARA",
        },
        {
          kota: "Kota Palu",
          provinsi: "SULAWESI TENGAH",
        },
        {
          kota: "Kota Makasar",
          provinsi: "SULAWESI SELATAN",
        },
        {
          kota: "Kota Kendari",
          provinsi: "SULAWESI TENGGARA",
        },
        {
          kota: "KOTA GORONTALO",
          provinsi: "GORONTALO",
        },
        {
          kota: "Kota Makasar",
          provinsi: "SULAWESI BARAT",
        },
        {
          kota: "Kota Ambon",
          provinsi: "MALUKU",
        },
        {
          kota: "Kota Ambon",
          provinsi: "MALUKU UTARA",
        },
        {
          kota: "Kota Jayapura",
          provinsi: "PAPUA BARAT",
        },
        {
          kota: "Kota Jayapura",
          provinsi: "PAPUA",
        },
      ],
      {}
    );
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("kota_bulogs", null, {});
  },
};
