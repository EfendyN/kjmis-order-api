const express = require("express");
const router = express.Router();

const { login, register, updatePass } = require("../controllers/auth");
const { get_user } = require("../controllers/user");
const { auth } = require("../middleware/auth");
const {
  // getKotaIndex,
  getProvinsiIndex,
  getOrderIndex,
  getOrderDetail,
  getKotaBulog,
  getKotaKabIndex,
  getBarangIndex,
  getTipeBarang,
  getHetBarang,
  getHistory,
  getHetIndex,
} = require("../controllers/get");
const {
  destroyOrder,
  destroyBarang,
  destroyRincianBarang,
} = require("../controllers/delete");
const {
  postOrder,
  postOrderIndex,
  postRincianOrder,
  postHet,
  postBarang,
  postRincianBarang,
} = require("../controllers/post");

const {
  putPayment,
  putHet,
  putOrder,
  putRincianBarang,
} = require("../controllers/put");

router.get("/", (req, res) => {
  res.send("KJMIS Letter & Form API");
});
// auth
router.post("/login", login);
router.post("/register", register);
router.put("/user", auth, updatePass);

// user
router.get("/user", auth, get_user);

//Provinsi & Kota
router.get("/provinsi", getProvinsiIndex);
router.get("/kotakab", getKotaKabIndex);
// router.post("/kota", getKotaIndex);
router.get("/bulog", getKotaBulog);

//Order & Payment
router.post("/orders", auth, postOrderIndex);
router.get("/order", auth, getOrderDetail);
router.post("/order", auth, postOrder);
router.post("/rincian", auth, postRincianOrder);
router.put("/payment", auth, putPayment);
router.delete("/order", auth, destroyOrder);
router.put("/order", auth, putOrder);
router.put("/rincian", auth, putRincianBarang);
router.post("/rincians", auth, postRincianBarang);
router.delete("/rincian", auth, destroyRincianBarang);

//Barang
router.get("/barang", auth, getBarangIndex);
router.get("/unit", auth, getTipeBarang);
router.post("/barang", auth, postBarang);
router.delete("/barang", auth, destroyBarang);

//Het
router.get("/het", getHetBarang);
router.get("/hets", getHetIndex);
router.post("/het", postHet);

// history
router.post("/history", getHistory);

module.exports = router;
