"use strict";
module.exports = (sequelize, DataTypes) => {
  const payment_log = sequelize.define(
    "payment_log",
    {
      type: DataTypes.ENUM("IN", "OUT"),
      bayar: DataTypes.BIGINT,
      selisih: DataTypes.BIGINT,
      tgl_bayar: DataTypes.DATE,
      payment_id: DataTypes.INTEGER,
    },
    {}
  );
  payment_log.associate = function (models) {
    // associations can be defined here
  };
  return payment_log;
};
