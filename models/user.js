"use strict";
module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define(
    "user",
    {
      name: DataTypes.STRING,
      username: DataTypes.STRING,
      password: DataTypes.STRING,
      level: DataTypes.ENUM("1", "2", "3"),
    },
    {}
  );
  user.associate = function (models) {
    // associations can be defined here
  };
  return user;
};
