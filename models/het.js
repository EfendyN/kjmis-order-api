"use strict";
module.exports = (sequelize, DataTypes) => {
  const het = sequelize.define(
    "het",
    {
      nama_barang: DataTypes.STRING,
      wilayah: DataTypes.STRING,
      het: DataTypes.INTEGER,
      status: DataTypes.ENUM("Aktif", "Tidak Aktif"),
      tgl_berlaku: DataTypes.DATEONLY,
    },
    {}
  );
  het.associate = function (models) {
    // associations can be defined here
  };
  return het;
};
