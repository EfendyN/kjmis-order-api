"use strict";
module.exports = (sequelize, DataTypes) => {
  const rincian_barang = sequelize.define(
    "rincian_barang",
    {
      nama_barang: DataTypes.STRING,
      unit: DataTypes.STRING,
      tipe_unit: DataTypes.STRING,
      jumlah: DataTypes.INTEGER,
      harga_perunit: DataTypes.INTEGER,
      total_harga: DataTypes.BIGINT,
      het: DataTypes.INTEGER,
      total_het: DataTypes.BIGINT,
      order_id: DataTypes.INTEGER,
    },
    {}
  );
  rincian_barang.associate = function (models) {
    // associations can be defined here
    rincian_barang.belongsTo(models.order, {
      as: "order",
      foreignKey: "order_id",
    });
  };
  return rincian_barang;
};
