'use strict';
module.exports = (sequelize, DataTypes) => {
  const wilayah = sequelize.define('wilayah', {
    wil_id: DataTypes.STRING,
    wil_prov: DataTypes.STRING,
    wil_kab: DataTypes.STRING,
    wil_kec: DataTypes.STRING,
    wil_desa: DataTypes.STRING
  }, {});
  wilayah.associate = function(models) {
    // associations can be defined here
  };
  return wilayah;
};