"use strict";
module.exports = (sequelize, DataTypes) => {
  const payment = sequelize.define(
    "payment",
    {
      bayar_bulog: DataTypes.BIGINT,
      tgl_bayar_bulog: DataTypes.DATEONLY,
      tagihan_bulog: DataTypes.BIGINT,
      balance_bulog: DataTypes.INTEGER,
      terima_bayar: DataTypes.BIGINT,
      tgl_terima_bayar: DataTypes.DATEONLY,
      tagihan_terima: DataTypes.BIGINT,
      balance_terima: DataTypes.INTEGER,
      order_id: DataTypes.INTEGER,
    },
    {}
  );
  payment.associate = function (models) {
    // associations can be defined here
    payment.belongsTo(models.order, {
      as: "order",
      foreignKey: "order_id",
    });
  };
  return payment;
};
