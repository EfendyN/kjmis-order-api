"use strict";
module.exports = (sequelize, DataTypes) => {
  const order = sequelize.define(
    "order",
    {
      asal_pesanan: DataTypes.STRING,
      tgl_surat_pesanan: DataTypes.DATEONLY,
      no_surat_pesanan: DataTypes.STRING,
      tujuan_supplier: DataTypes.STRING,
      tujuan_provinsi: DataTypes.STRING,
      no_surat_tujuan: DataTypes.STRING,
      tgl_surat_tujuan: DataTypes.DATEONLY,
      daerah_tujuan: DataTypes.STRING,
      daerah_tujuan_bulog: DataTypes.STRING,
      tgl_pengambilan: DataTypes.DATEONLY,
      payment_id: DataTypes.INTEGER,
      status: DataTypes.ENUM("Lunas", "Tunggakan"),
    },
    {}
  );
  order.associate = function (models) {
    // associations can be defined here
    order.belongsTo(models.payment, {
      as: "payment",
      foreignKey: "payment_id",
    });
  };
  return order;
};
