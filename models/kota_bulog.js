"use strict";
module.exports = (sequelize, DataTypes) => {
  const kota_bulog = sequelize.define(
    "kota_bulog",
    {
      kota: DataTypes.STRING,
      provinsi: DataTypes.STRING,
    },
    {}
  );
  kota_bulog.associate = function (models) {
    // associations can be defined here
  };
  return kota_bulog;
};
