const models = require("../models");
const Sequelize = require("sequelize");
const { Op } = Sequelize;
const Order = models.order;
const Payment = models.payment;
const Rincian = models.rincian_barang;
const Bulog = models.kota_bulog;
const Het = models.het;
const PaymentLog = models.payment_log;

exports.putPayment = async (req, res) => {
  try {
    const { payment_id, type } = req.query;
    const { pay } = req.body;
    const paymentDetail = await Payment.findOne({ where: { id: payment_id } });
    if (paymentDetail) {
      const payment = await Payment.update(pay, {
        where: { id: payment_id },
      });
      if (payment) {
        const bBulog = pay.bayar_bulog - paymentDetail.tagihan_bulog;
        const bTerima = pay.terima_bayar - paymentDetail.tagihan_terima;
        const balance = await Payment.update(
          { balance_bulog: bBulog, balance_terima: bTerima },
          {
            where: { id: payment_id },
          }
        );
        const data = await Payment.findOne({ where: { id: payment_id } });

        const allPaymentLog = await PaymentLog.findAll({
          where: { payment_id },
        });

        let result_paymentLog = allPaymentLog.map(({ bayar }) => bayar);

        let sum_log = result_paymentLog.reduce(function (a, b) {
          return a + b;
        }, 0);

        const bayar = type === "IN" ? data.terima_bayar : data.bayar_bulog;
        const x =
          type === "IN"
            ? paymentDetail.terima_bayar
            : paymentDetail.bayar_bulog;
        const selisih = bayar - x;

        console.log({ selisih, bayar });

        const paymentLog = await PaymentLog.create({
          type,
          bayar,
          selisih,
          tgl_bayar:
            type === "IN" ? data.tgl_terima_bayar : data.tgl_bayar_bulog,
          payment_id,
        });

        if (data.balance_bulog >= 0 && data.balance_terima >= 0) {
          const orderUpdate = await Order.update(
            { status: "Lunas" },
            { where: { id: data.order_id } }
          );

          const order = await Order.findOne({ where: { id: data.order_id } });
          res.send({ data: { data, order, paymentLog, allPaymentLog } });
        } else {
          const orderUpdate = await Order.update(
            { status: "Tunggakan" },
            { where: { id: data.order_id } }
          );
          const order = await Order.findOne({ where: { id: data.order_id } });
          res.send({ data: { data, order, paymentLog, allPaymentLog } });
        }
      }
    }
  } catch (err) {
    console.log(err);
  }
};

exports.putHet = async (req, res) => {
  try {
    const { id } = req.query;
    const { data } = req.body;
    const { nama_barang, wilayah, het } = data;
    const date = new Date();

    const findHet = await Het.findAll({ where: { nama_barang, wilayah } });
    let result = findHet.map(({ id }) => id);
    console.log({ result });

    const create = await Het.create({
      nama_barang,
      wilayah,
      het,
      tgl_berlaku: date,
      status: "Aktif",
    });

    if (result.length >= 1) {
      console.log(result.length, "Adasdasd asd asdas ");
      const updateHet = await Het.update(
        { status: "Tidak Aktif" },
        { where: { id: { [Op.in]: result } } }
      );
      const findUpdateHet = await Het.findAll({
        where: { id: { [Op.in]: result } },
      });
      res.json({ message: "success update", create, findUpdateHet });
    } else {
      res.json({ message: "success", create, findHet });
    }
  } catch (err) {
    console.log(err);
  }
};

exports.putOrder = async (req, res) => {
  try {
    const { id } = req.query;
    const { dataOrder } = req.body;
    const { tujuan_provinsi } = dataOrder;
    const updateOrder = await Order.update(dataOrder, { where: { id } });
    const findOrder = await Order.findOne({ where: { id } });

    const bulog = await Bulog.findOne({
      where: { provinsi: tujuan_provinsi },
    });

    const updateDaerahBulog = await Order.update(
      {
        daerah_tujuan_bulog:
          findOrder.tujuan_supplier === "BULOG"
            ? bulog.kota
            : dataOrder .daerah_tujuan_bulog,
      },
      { where: { id } }
    );
    const order = await Order.findOne({ where: { id } });
    res.send({ message: "Success", data: order });
  } catch (err) {
    console.log(err);
  }
};

exports.putRincianBarang = async (req, res) => {
  try {
    const { id } = req.query;
    const { dataRincian } = req.body;
    const { unit, jumlah, harga_perunit, het } = dataRincian;
    const tharga = unit * jumlah * harga_perunit;
    const thet = unit * jumlah * het;

    const updateRincian = await Rincian.update(
      { ...dataRincian, total_harga: tharga, total_het: thet },
      { where: { id } }
    );

    const rincian = await Rincian.findOne({ where: { id } });

    const allRician = await Rincian.findAll({
      where: { order_id: rincian.order_id },
    });

    if (updateRincian) {
      let result_harga = allRician.map(({ total_harga }) => total_harga);
      let result_het = allRician.map(({ total_het }) => total_het);

      let sum_harga = result_harga.reduce(function (a, b) {
        return a + b;
      }, 0);

      let sum_het = result_het.reduce(function (a, b) {
        return a + b;
      }, 0);

      const tagihantrm = sum_harga + (sum_het - sum_harga) * (100 / 100);
      const paymentUpdate = await Payment.update(
        { tagihan_bulog: sum_harga, tagihan_terima: tagihantrm },
        {
          where: { order_id: rincian.order_id },
        }
      );

      const pay = await Payment.findOne({
        where: { order_id: rincian.order_id },
      });

      const bBulog = pay.bayar_bulog - pay.tagihan_bulog;
      const bTerima = pay.terima_bayar - pay.tagihan_terima;
      const balance = await Payment.update(
        { balance_bulog: bBulog, balance_terima: bTerima },
        {
          where: { order_id: rincian.order_id },
        }
      );

      const dataPayment = await Payment.findOne({
        where: { order_id: rincian.order_id },
      });

      if (dataPayment.balance_bulog >= 0 && dataPayment.balance_terima >= 0) {
        const orderUpdate = await Order.update(
          { status: "Lunas" },
          { where: { id: rincian.order_id } }
        );

        const order = await Order.findOne({ where: { id: rincian.order_id } });
        res.send({ data: { dataPayment, order, rincian } });
      } else {
        const orderUpdate = await Order.update(
          { status: "Tunggakan" },
          { where: { id: rincian.order_id } }
        );
        const order = await Order.findOne({ where: { id: rincian.order_id } });
        res.send({ data: { dataPayment, order, rincian } });
      }
    }
  } catch (err) {
    console.log(err);
  }
};
