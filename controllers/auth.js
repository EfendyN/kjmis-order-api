const jwt = require("jsonwebtoken");
const models = require("../models");
const User = models.user;
const bcrypt = require("bcrypt");
const sequelize = require("sequelize");
const saltRounds = 10;
// LOGOIN
exports.login = async (req, res) => {
  try {
    const { username, password } = req.body;
    const user = await User.findOne({ where: { username } });

    if (user) {
      const result = await bcrypt.compare(password, user.password);
      if (result) {
        const token = jwt.sign(
          { user_id: user.id, status: user.status },
          process.env.SECRET_KEY
        );
        res.status(200).send({
          username,
          token,
          status: user.status,
          message: "success login",
        });
      } else {
        res.send({ message: "Password Salah" });
      }
    } else {
      res.send({ message: "Username atau Password Salah" });
    }
  } catch (err) {
    console.log(err);
  }
};

exports.register = async (req, res) => {
  const { data } = req.body;
  const { username } = data;
  const salt = await bcrypt.genSalt(saltRounds);

  try {
    if (!data) {
      res.json({
        status: false,
        message: "fail",
      });
    } else {
      const check = await User.findOne({
        where: { [sequelize.Op.or]: { username } },
      });
      if (check) {
        res.json({
          status: false,
          message: "Email or Username Already Registered",
        });
      } else {
        const regUser = await User.create(data);
        const hash = await bcrypt.hash(regUser.password, salt);
        const updatePass = await User.update(
          {
            password: hash,
            level: 3,
          },
          { where: { id: regUser.id } }
        );
        if (updatePass) {
          const token = jwt.sign(
            { user_id: regUser.id },
            process.env.SECRET_KEY
          );
          res.send({
            username,
            token,
            status: true,
            message: "Register Success",
          });
        } else {
          res.json({ status: false, message: "Invalid Register" });
        }
      }
    }
  } catch (err) {
    console.log(err);
  }
};

exports.updatePass = async (req, res) => {
  try {
    const { dataUpdate } = req.body;
    const { id } = req.query;
    const { password, passwordLama } = dataUpdate;
    const salt = await bcrypt.genSalt(saltRounds);

    const user = await User.findOne({ where: { id } });

    if (user) {
      const check = await bcrypt.compare(passwordLama, user.password);
      if (check) {
        const hash = await bcrypt.hash(password, salt);
        const updatePass = await User.update(
          { password: hash },
          { where: { id } }
        );
        res.json({ message: "Success" });
      } else {
        res.json({ message: "Password Salah" });
      }
    }
  } catch (err) {
    console.log(err);
  }
};
