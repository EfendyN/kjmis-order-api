const models = require("../models");
const Sequelize = require("sequelize");
const { Op } = Sequelize;
const Order = models.order;
const Rincian = models.rincian_barang;
const User = models.user;
const Barang = models.barang;
const Payment = models.payment;

const bcrypt = require("bcrypt");
const saltRounds = 10;

exports.destroyOrder = async (req, res) => {
  const { order_id, password } = req.query;

  try {
    const user = await User.findOne({ where: { id: req.user } });
    if (user) {
      const result = await bcrypt.compare(password, user.password);
      if (result) {
        if (user.level === "1") {
          const destroyOrder = await Order.destroy({ where: { id: order_id } });
          const destroyRincian = await Rincian.destroy({ where: { order_id } });
          const destroyPayment = await Payment.destroy({ where: { order_id } });
          res.send({ data: { message: "Data Berhasil Dihapus" } });
        } else {
          res.send({ data: { message: "Data Gagal Dihapus" } });
        }
      } else {
        res.send({ data: { message: "Password Salah" } });
      }
    }
  } catch (err) {
    console.log(err);
  }
};

exports.destroyBarang = async (req, res) => {
  const { id } = req.query;
  try {
    const destroyBarang = await Barang.destroy({ where: { id } });

    res.json({ message: "success" });
  } catch (err) {
    console.log(err);
  }
};

exports.destroyRincianBarang = async (req, res) => {
  const { id } = req.query;
  try {
    const findRincian = await Rincian.findOne({
      where: { id },
    });

    const destroyRincian = await Rincian.destroy({
      where: { id },
    });

    if (destroyRincian) {
      const findAllRincian = await Rincian.findAll({
        where: { order_id: findRincian.order_id },
      });

      let result_harga = findAllRincian.map(({ total_harga }) => total_harga);
      let result_het = findAllRincian.map(({ total_het }) => total_het);

      let sum_harga = result_harga.reduce(function (a, b) {
        return Number(a) + Number(b);
      }, 0);

      let sum_het = result_het.reduce(function (a, b) {
        return Number(a) + Number(b);
      }, 0);

      const tagihantrm = sum_harga + (sum_het - sum_harga) * (30 / 100);
      const paymentUpdate = await Payment.update(
        { tagihan_bulog: sum_harga, tagihan_terima: tagihantrm },
        {
          where: { order_id: findRincian.order_id },
        }
      );

      const pay = await Payment.findOne({
        where: { order_id: findRincian.order_id },
      });

      const bBulog = pay.bayar_bulog - pay.tagihan_bulog;
      const bTerima = pay.terima_bayar - pay.tagihan_terima;

      const balance = await Payment.update(
        { balance_bulog: bBulog, balance_terima: bTerima },
        {
          where: { order_id: findRincian.order_id },
        }
      );

      const dataPayment = await Payment.findOne({
        where: { order_id: findRincian.order_id },
      });

      if (dataPayment.balance_bulog >= 0 && dataPayment.balance_terima >= 0) {
        const orderUpdate = await Order.update(
          { status: "Lunas" },
          { where: { id: findRincian.order_id } }
        );
        const order = await Order.findOne({
          where: { id: findRincian.order_id },
        });
        res.send({ data: { dataPayment, order, findAllRincian } });
      } else {
        const orderUpdate = await Order.update(
          { status: "Tunggakan" },
          { where: { id: findRincian.order_id } }
        );
        const order = await Order.findOne({
          where: { id: findRincian.order_id },
        });
        res.send({ data: { dataPayment, order, findAllRincian } });
      }
    }
  } catch (err) {
    console.log(err);
  }
};
