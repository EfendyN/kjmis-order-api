const models = require("../models");
const Sequelize = require("sequelize");
const { Op } = Sequelize;
const Orders = models.order;
const Payment = models.payment;
const PaymentLog = models.payment_log;
const Rincian = models.rincian_barang;

const Wilayah = models.wilayah;
// const Provinsi = models.provinsi;
// const Kota = models.kota;
const Bulog = models.kota_bulog;

const Barang = models.barang;
const Het = models.het;

exports.getProvinsiIndex = async (req, res) => {
  try {
    const provinsi = await Wilayah.findAll({
      attributes: [
        [Sequelize.fn("DISTINCT", Sequelize.col("wil_prov")), "wil_prov"],
      ],
      order: [["wil_prov", "ASC"]],
    });
    res.send({ data: { provinsi } });
  } catch (err) {
    console.log(err);
  }
};

exports.getKotaKabIndex = async (req, res) => {
  const { x } = req.query;
  try {
    const provinsi = await Wilayah.findAll({
      where: { wil_prov: { [Op.substring]: x } },
      attributes: [
        [Sequelize.fn("DISTINCT", Sequelize.col("wil_kab")), "wil_kab"],
      ],
      order: [["wil_kab", "ASC"]],
    });
    res.send({ data: { provinsi } });
  } catch (err) {
    console.log(err);
  }
};

// exports.getKotaIndex = async (req, res) => {
//   const { provinsi } = req.query;
//   console.log({ provinsi });
//   try {
//     if (provinsi === "" || provinsi === undefined) {
//       const kota = await Kota.findAll({
//         include: [{ model: Provinsi, as: "provinsi" }],
//         attributes: {
//           exclude: [, "createdAt", "updatedAt"],
//         },
//         order: [["nama", "ASC"]],
//       });
//       res.send({ data: kota });
//     } else {
//       const kota = await Kota.findAll(
//         { where: { provinsi_id: provinsi } },
//         {
//           include: [{ model: Provinsi, as: "provinsi" }],
//           attributes: {
//             exclude: [, "createdAt", "updatedAt"],
//           },
//         }
//       );
//       res.send({ data: kota });
//     }
//   } catch (err) {
//     console.log(err);
//   }
// };

exports.getKotaBulog = async (req, res) => {
  try {
    const provinsi = await Bulog.findAll({
      attributes: {
        exclude: [, "createdAt", "updatedAt"],
      },
      order: [["provinsi", "ASC"]],
    });
    res.send({ data: provinsi });
  } catch (err) {
    console.log(err);
  }
};

// exports.getOrderIndex = async (req, res) => {
//   try {
//     const orders = await Orders.findAll({
//       attributes: {
//         exclude: [, "createdAt", "updatedAt"],
//       },
//     });
//     res.send({ data: orders });
//   } catch (err) {
//     console.log(err);
//   }
// };

exports.getOrderDetail = async (req, res) => {
  try {
    const { order_id } = req.query;
    const orderDetail = await Orders.findOne({
      where: { id: order_id },
      attributes: { exclude: ["createdAt", "updatedAt"] },
    });
    if (orderDetail === null) {
      res.status(200).send({ message: "no data found", data: orderDetail });
    } else {
      const rincian = await Rincian.findAll({
        where: { order_id },
      });
      const payment = await Payment.findOne({
        where: { order_id },
      });
      const payment_log_in = await PaymentLog.findAll({
        where: { payment_id: payment.id, type: "IN" },
      });
      const payment_log_out = await PaymentLog.findAll({
        where: { payment_id: payment.id, type: "OUT" },
      });
      res.status(200).send({
        message: "success to find data",
        data: {
          order: orderDetail,
          rincian: rincian,
          payment: payment,
          payment_log_in,
          payment_log_out,
        },
      });
    }
  } catch (err) {
    console.log(err);
  }
};

exports.getBarangIndex = async (req, res) => {
  try {
    const barang = await Barang.findAll({
      attributes: {
        exclude: [, "createdAt", "updatedAt"],
      },
      order: [["nama", "ASC"]],
    });
    res.send({ data: barang });
  } catch (err) {
    console.log(err);
  }
};

exports.getTipeBarang = async (req, res) => {
  const { x } = req.query;
  try {
    const barang = await Barang.findOne({
      where: { nama: x },
      attributes: {
        exclude: ["createdAt", "updatedAt"],
      },
    });
    res.send({ data: barang });
  } catch (err) {
    console.log(err);
  }
};

exports.getHetBarang = async (req, res) => {
  const { p, b } = req.query;
  try {
    const het = await Het.findOne({
      where: { nama_barang: b, wilayah: p },
      attributes: {
        exclude: ["createdAt", "updatedAt"],
      },
    });
    res.send({ data: het });
  } catch (err) {
    console.log(err);
  }
};

exports.getHetIndex = async (req, res) => {
  const { nama, wilayah, status } = req.query;
  try {
    const het = await Het.findAll({
      where: {
        nama_barang: { [Op.substring]: nama },
        wilayah: { [Op.substring]: wilayah },
        status: { [Op.startsWith]: status },
      },
      order: [["nama_barang", "ASC"]],
    });
    res.send({ data: het });
  } catch (err) {
    console.log(err);
  }
};

exports.getHistory = async (req, res) => {
  const { values } = req.body;
  const { barang, from, to, supplier, provinsi, daerah } = values;
  console.log({ barang, from, to, provinsi, daerah, values });
  try {
    const order = await Orders.findAll({
      where: {
        [Op.and]: {
          tgl_surat_pesanan: { [Op.between]: [from, to] },
          // tujuan_supplier:
          //   supplier.length > 0
          //     ? { [Op.in]: supplier }
          //     : { [Op.substring]: supplier },
          tujuan_provinsi:
            provinsi.length > 0
              ? { [Op.in]: provinsi }
              : { [Op.substring]: provinsi },
          daerah_tujuan:
            daerah.length > 0
              ? { [Op.in]: daerah }
              : { [Op.substring]: daerah },
        },
      },
      attributes: { exclude: ["createdAt", "updatedAt"] },
    });

    if (order) {
      let result = order.map(({ id }) => id);
      console.log({ result });

      const payment = await Payment.findAll({
        where: { order_id: { [Op.in]: result } },
        attributes: { exclude: ["createdAt", "updatedAt"] },
      });

      let result_pembayaran = payment.map(({ bayar_bulog }) => bayar_bulog);
      let result_penerimaan = payment.map(({ terima_bayar }) => terima_bayar);

      const rincian = await Rincian.findAll({
        where: {
          [Op.and]: {
            order_id: { [Op.in]: result },
            nama_barang: { [Op.in]: barang },
          },
        },
        include: {
          model: Orders,
          as: "order",
          attributes: [
            "id",
            "asal_pesanan",
            "tgl_surat_pesanan",
            "tujuan_supplier",
            "tujuan_provinsi",
            "no_surat_tujuan",
            "tgl_surat_tujuan",
            "daerah_tujuan",
            "tgl_pengambilan",
            "status",
          ],
        },
        order: [["nama_barang", "ASC"]],
        attributes: { exclude: ["createdAt", "updatedAt"] },
      });

      let result_orderid = rincian.map(({ order_id }) => order_id);

      let result_unit = rincian.map(({ unit }) => unit);
      let result_jumlah = rincian.map(({ jumlah }) => jumlah);

      let sum_unit = result_unit.reduce(function (a, b) {
        return Number(a) + Number(b);
      }, 0);
      let sum_jumlah = result_jumlah.reduce(function (a, b) {
        return Number(a) + Number(b);
      }, 0);

      let sum_pembayaran = result_pembayaran.reduce(function (a, b) {
        return Number(a) + Number(b);
      }, 0);
      let sum_penerimaan = result_penerimaan.reduce(function (a, b) {
        return Number(a) + Number(b);
      }, 0);

      // console.log({ result_penerimaan }, { result_pembayaran });
      const balance = sum_penerimaan - sum_pembayaran;
      const totalUnit = sum_unit * sum_jumlah;

      const resultOrder = await Orders.findAll({
        where: { id: { [Op.in]: result_orderid }, status: "LUNAS" },
      });

      let resultOrderLunas = resultOrder.map(({ id }) => id);

      const rincianLunas = await Rincian.findAll({
        where: { order_id: { [Op.in]: resultOrderLunas } },
      });

      let resultRincianLunasUnit = rincianLunas.map(({ unit }) => unit);
      let resultRincianLunasJml = rincianLunas.map(({ jumlah }) => jumlah);

      const totalUnitLunas =
        totalUnit - resultRincianLunasUnit * resultRincianLunasJml;

      // const paymentx = await Payment.findAll(
      //   {
      //     where: {
      //       order_id: { [Op.in]: result_orderid },
      //     },
      //     include: {
      //       model: Orders,
      //       as: "order",
      //       attributes: [
      //         "id",
      //         "asal_pesanan",
      //         "tgl_surat_pesanan",
      //         "tujuan_supplier",
      //         "tujuan_provinsi",
      //         "no_surat_tujuan",
      //         "tgl_surat_tujuan",
      //         "daerah_tujuan",
      //         "tgl_pengambilan",
      //         "status",
      //       ],
      //     },
      //     order: [["id", "DESC"]],
      //   },
      //   {
      //     attributes: {
      //       exclude: ["createdAt", "updatedAt"],
      //     },
      //   }
      // );

      if (rincian.length > 0) {
        res.send({
          data: {
            order: rincian,
            // total_penerimaan: sum_penerimaan,
            // total_pembayaran: sum_pembayaran,
            // balance,
            // total_unit: totalUnit,
            // total_unit_lunas: totalUnitLunas,
            message: "Data Ditemukan",
          },
        });
      } else {
        res.send({
          data: {
            order: rincian,
            // total_penerimaan: sum_penerimaan,
            // total_pembayaran: sum_pembayaran,
            // balance,
            // total_unit: totalUnit,
            // total_unit_lunas: totalUnitLunas,
            message: "Data Tidak Ditemukan",
          },
        });
      }
    }
  } catch (err) {
    console.log(err);
  }
};
