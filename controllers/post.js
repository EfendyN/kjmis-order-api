const models = require("../models");
const Sequelize = require("sequelize");
const { Op } = Sequelize;
const Order = models.order;
const Payment = models.payment;
const Rincian = models.rincian_barang;
const Bulog = models.kota_bulog;
const Het = models.het;
const Barang = models.barang;

exports.postOrderIndex = async (req, res) => {
  const {
    asal_pesanan,
    tgl_surat_pesanan,
    no_surat_pesanan,
    tujuan_provinsi,
    no_surat_tujuan,
    tgl_surat_tujuan,
    daerah_tujuan,
    tgl_pengambilan,
    status,
    tujuan_supplier,
  } = req.query;
  try {
    const orders = await Order.findAll(
      {
        where: {
          asal_pesanan: { [Op.substring]: asal_pesanan },
          tgl_surat_pesanan: { [Op.substring]: tgl_surat_pesanan },
          no_surat_pesanan: { [Op.substring]: no_surat_pesanan },
          tujuan_provinsi: { [Op.substring]: tujuan_provinsi },
          // tujuan_supplier: { [Op.substring]: tujuan_supplier },
          no_surat_tujuan: { [Op.substring]: no_surat_tujuan },
          tgl_surat_tujuan: { [Op.substring]: tgl_surat_tujuan },
          daerah_tujuan: { [Op.substring]: daerah_tujuan },
          tgl_pengambilan: { [Op.substring]: tgl_pengambilan },
          status: { [Op.substring]: status },
        },
        include: {
          model: Payment,
          as: "payment",
          attributes: ["id", "balance_terima"],
        },
        order: [["id", "DESC"]],
      },
      {
        attributes: {
          exclude: ["createdAt", "updatedAt"],
        },
      }
    );
    let result = orders.map(({ id }) => id);

    const payment = await Payment.findAll(
      {
        where: {
          order_id: { [Op.in]: result },
        },
        include: {
          model: Order,
          as: "order",
          attributes: [
            "id",
            "asal_pesanan",
            "tgl_surat_pesanan",
            "tujuan_provinsi",
            "tujuan_supplier",
            "no_surat_tujuan",
            "tgl_surat_tujuan",
            "daerah_tujuan",
            "tgl_pengambilan",
            "status",
          ],
        },
        order: [["id", "DESC"]],
      },
      {
        attributes: {
          exclude: ["createdAt", "updatedAt"],
        },
      }
    );
    res.send({ data: payment });
  } catch (err) {
    console.log(err);
  }
};

exports.postRincianOrder = async (req, res) => {
  const { id } = req.query;
  try {
    const rincian = await Rincian.findAll(
      {
        where: { order_id: id },
        order: [["id", "ASC"]],
      },
      {
        attributes: {
          exclude: ["createdAt", "updatedAt"],
        },
      }
    );
    res.send({ data: rincian });
  } catch (err) {
    console.log(err);
  }
};

exports.postOrder = async (req, res) => {
  const { order, list_rincian } = req.body;
  try {
    const createOrder = await Order.create(order);
    const createRincian = await Rincian.bulkCreate(list_rincian);

    if (createOrder) {
      const bulog = await Bulog.findOne({
        where: { provinsi: createOrder.tujuan_provinsi },
      });
      const updateStatusOrder = await Order.update(
        {
          daerah_tujuan_bulog:
            createOrder.tujuan_supplier === "BULOG"
              ? bulog.kota
              : createOrder.daerah_tujuan_bulog,
          status: "Tunggakan",
        },
        { where: { id: createOrder.id } }
      );

      let result = createRincian.map(({ id }) => id);
      let result_harga = createRincian.map(({ total_harga }) => total_harga);
      let result_het = createRincian.map(({ total_het }) => total_het);

      let sum_harga = result_harga.reduce(function (a, b) {
        return a + b;
      }, 0);

      let sum_het = result_het.reduce(function (a, b) {
        return a + b;
      }, 0);

      const updateRincian = await Rincian.update(
        {
          order_id: createOrder.id,
        },
        {
          where: {
            id: { [Op.in]: result },
          },
        }
      );

      const tagihantrm = sum_harga + (sum_het - sum_harga) * (100 / 100);
      const createPayment = await Payment.create({
        tagihan_bulog: sum_harga,
        tagihan_terima: tagihantrm,
        order_id: createOrder.id,
      });
      res.status(200).json({ data: { createOrder, createPayment } });
    }
  } catch (err) {
    console.log(err);
  }
};

exports.postHet = async (req, res) => {
  const { dataHet } = req.body;
  const { nama_barang, wilayah } = dataHet;
  try {
    const findHet = await Het.findOne({
      where: { nama_barang, wilayah, status: "Aktif" },
    });
    if (findHet !== null) {
      const updateStatus = await Het.update(
        {
          status: "Tidak Aktif",
        },
        { where: { id: findHet.id } }
      );
    }

    const het = await Het.create(dataHet);
    const updateStatusNewHet = await Het.update(
      {
        status: "Aktif",
        tgl_berlaku: new Date(),
      },
      { where: { id: het.id } }
    );
    const newHet = await Het.findOne({ where: { id: het.id } });

    if (findHet !== null) {
      const oldHet = await Het.findOne({ where: { id: findHet.id } });
      res.send({ data: { newHet, oldHet } });
    } else {
      res.send({ data: { newHet } });
    }
  } catch (err) {
    console.log(err);
  }
};

exports.postBarang = async (req, res) => {
  const { dataBarang } = req.body;
  const { nama } = dataBarang;
  try {
    const findBarang = await Barang.findOne({ where: { nama } });
    if (findBarang !== null) {
      res.json({ data: { message: "ada" } });
    } else {
      const createBarang = await Barang.create(dataBarang);
      res.json({ data: { message: "ok", data: createBarang } });
    }
    res.json({ findBarang });
  } catch (err) {
    console.log(err);
  }
};

exports.postRincianBarang = async (req, res) => {
  const { order_id } = req.query;
  const { dataRincian } = req.body;
  const { unit, jumlah, harga_perunit, het } = dataRincian;
  const total_harga = unit * jumlah * harga_perunit;
  const total_het = unit * jumlah * het;
  try {
    const createRincian = await Rincian.create({
      ...dataRincian,
      total_harga,
      total_het,
      order_id,
    });
    if (createRincian) {
      const findAllRincian = await Rincian.findAll({ where: { order_id } });

      let result_harga = findAllRincian.map(({ total_harga }) => total_harga);
      let result_het = findAllRincian.map(({ total_het }) => total_het);

      let sum_harga = result_harga.reduce(function (a, b) {
        return Number(a) + Number(b);
      }, 0);

      let sum_het = result_het.reduce(function (a, b) {
        return Number(a) + Number(b);
      }, 0);

      const tagihantrm = sum_harga + (sum_het - sum_harga) * (100 / 100);
      const paymentUpdate = await Payment.update(
        { tagihan_bulog: sum_harga, tagihan_terima: tagihantrm },
        {
          where: { order_id },
        }
      );

      const pay = await Payment.findOne({
        where: { order_id },
      });

      const bBulog = pay.bayar_bulog - pay.tagihan_bulog;
      const bTerima = pay.terima_bayar - pay.tagihan_terima;

      const balance = await Payment.update(
        { balance_bulog: bBulog, balance_terima: bTerima },
        {
          where: { order_id },
        }
      );

      const dataPayment = await Payment.findOne({
        where: { order_id },
      });

      if (dataPayment.balance_bulog >= 0 && dataPayment.balance_terima >= 0) {
        const orderUpdate = await Order.update(
          { status: "Lunas" },
          { where: { id: order_id } }
        );
        const order = await Order.findOne({ where: { id: order_id } });
        res.send({ data: { dataPayment, order, createRincian } });
      } else {
        const orderUpdate = await Order.update(
          { status: "Tunggakan" },
          { where: { id: order_id } }
        );
        const order = await Order.findOne({ where: { id: order_id } });
        res.send({ data: { dataPayment, order, createRincian } });
      }
    }
  } catch (err) {
    console.log(err);
  }
};
