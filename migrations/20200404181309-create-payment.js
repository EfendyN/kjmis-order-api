'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('payments', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      bayar_bulog: {
        type: Sequelize.BIGINT
      },
      tgl_bayar_bulog: {
        type: Sequelize.DATEONLY
      },
      tagihan_bulog: {
        type: Sequelize.BIGINT
      },
      balance_bulog: {
        type: Sequelize.INTEGER
      },
      terima_bayar: {
        type: Sequelize.BIGINT
      },
      tgl_terima_bayar: {
        type: Sequelize.DATEONLY
      },
      tagihan_terima: {
        type: Sequelize.BIGINT
      },
      balance_terima: {
        type: Sequelize.INTEGER
      },
      order_id: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('payments');
  }
};