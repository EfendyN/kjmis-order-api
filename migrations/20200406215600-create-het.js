"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("hets", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      nama_barang: {
        type: Sequelize.STRING,
      },
      wilayah: {
        type: Sequelize.STRING,
      },
      het: {
        type: Sequelize.INTEGER,
      },
      status: {
        type: Sequelize.ENUM("Aktif", "Tidak Aktif"),
      },
      tgl_berlaku: {
        type: Sequelize.DATEONLY,
      },
      createdAt: {
        allowNull: true,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("hets");
  },
};
