"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("orders", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      asal_pesanan: {
        type: Sequelize.STRING,
      },
      tgl_surat_pesanan: {
        type: Sequelize.DATEONLY,
      },
      no_surat_pesanan: {
        type: Sequelize.STRING,
      },
      tujuan_supplier: {
        type: Sequelize.STRING,
      },
      tujuan_provinsi: {
        type: Sequelize.STRING,
      },
      no_surat_tujuan: {
        type: Sequelize.STRING,
      },
      tgl_surat_tujuan: {
        type: Sequelize.DATEONLY,
      },
      daerah_tujuan: {
        type: Sequelize.STRING,
      },
      daerah_tujuan_bulog: {
        type: Sequelize.STRING,
      },
      tgl_pengambilan: {
        type: Sequelize.DATEONLY,
      },
      payment_id: {
        type: Sequelize.INTEGER,
      },
      status: {
        type: Sequelize.ENUM("Lunas", "Tunggakan"),
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("orders");
  },
};
