"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("rincian_barangs", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      nama_barang: {
        type: Sequelize.STRING,
      },
      unit: {
        type: Sequelize.INTEGER,
      },
      tipe_unit: {
        type: Sequelize.STRING,
      },
      jumlah: {
        type: Sequelize.INTEGER,
      },
      harga_perunit: {
        type: Sequelize.INTEGER,
      },
      total_harga: {
        type: Sequelize.BIGINT,
      },
      het: {
        type: Sequelize.INTEGER,
      },
      total_het: {
        type: Sequelize.BIGINT,
      },
      order_id: {
        type: Sequelize.INTEGER,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("rincian_barangs");
  },
};
