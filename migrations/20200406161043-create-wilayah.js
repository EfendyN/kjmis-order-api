"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("wilayahs", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      wil_id: {
        type: Sequelize.STRING,
      },
      wil_prov: {
        type: Sequelize.STRING,
      },
      wil_kab: {
        type: Sequelize.STRING,
      },
      wil_kec: {
        type: Sequelize.STRING,
      },
      wil_desa: {
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: true,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("wilayahs");
  },
};
